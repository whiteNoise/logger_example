import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.concurrent.TimeUnit;

/**
 * Pseudo-useful logger which grabs lines from txt file and prints log messages
 */
public class LoggingLogic implements Runnable{
    private boolean run = true;
    private static final Logger log = LoggerFactory.getLogger(LoggingLogic.class);
    public void run() {
        try{
            printToLog();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Generates pseudo-random logging messages
     * @param rand
     * @param msg
     */
    private void doOrder(int rand, String msg) {
        try {
            if (rand == 0) {
                log.info(msg);
            } else if (rand == 1) {
                log.error(msg);
            } else {
                log.warn(msg);
            }
        TimeUnit.SECONDS.sleep(1);    
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Takes line from dummy.txt and wtites formatted message to logs.log
     * Format: %d{yyyy-MM-dd HH:mm:ss} %p "Имитатор:" %m %n
     */
    public void printToLog() {
        try {
            File file = new File(new File(".").getAbsolutePath()+"\\src\\main\\java\\dummy.txt");
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line;
            double max = 2.0;
            while ((line = bufferedReader.readLine()) != null) {
                int n = (int) ((Math.random() * ++max) % 3);
                doOrder(n, "Имитатор: "+line);
            }
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}