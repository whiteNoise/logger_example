import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implements dynamic parsing log file
 * Continues reading file from last point of reading
 * Uses RandomAccessFile to read from
 */
public class Tailer implements Runnable {
    private File file;
    private int runEvery;
    private long lastPosition = 0;
    private boolean run = true;
    private String option;
    public Tailer(String inputFile,String option, int interval) {
        file = new File(inputFile);
        this.runEvery = interval;
        this.option = option;
    }

    public void stop() {
        run = false;
    }

    public void run() {
        try {
            Pattern pattern = Pattern.compile(option);
            String c;
            while (run) {

                Thread.sleep(runEvery);
                long fileLength = file.length();

                if (fileLength > lastPosition) {
                    RandomAccessFile fh = new RandomAccessFile(file, "r");
                    fh.seek(lastPosition);

                    while ((c = fh.readLine()) != null) {

                        String str2 = new String(c.getBytes("ISO-8859-1"), "UTF8");
                        Matcher matcher = pattern.matcher(str2);

                        if (matcher.find()) {
                            System.out.println(str2.replaceAll("WARN","WARNING"));
                            TimeUnit.SECONDS.sleep(1);
                        }
                    }

                    lastPosition = fh.getFilePointer();
                    fh.close();
                }
            }
        } catch (Exception e) {
            stop();
        }
    }
}