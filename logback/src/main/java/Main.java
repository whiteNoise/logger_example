import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by alex on 25.05.2019.
 * This app uses slf4j and BackLogger for level-filtered logging
 */
public class Main {
    private static LoggingLogic logic;
    private static final String logPath="logs.log";

    public static void main(String[] args) {
        InputStreamReader reader = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(reader);
        Tailer tailer = new Tailer(logPath, formRegexp(formOptions(br)), 1000);
        logic = new LoggingLogic();

        ExecutorService executor = Executors.newFixedThreadPool(2);
        executor.execute(logic);
        executor.execute(tailer);
    }

    /**
     * @param reader
     *Creates String array of options for formRegexp()
     */

    public static String[] formOptions(BufferedReader reader){
        String[] options = new String[3];
        try {
            for (int i = 0; i < 3; i++) {
                options[i] = reader.readLine();
            }
            return options;
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param input
     * Forms options regexp for Tailer.run()
     * INPUT: WARN
     *        ERROR
     *
     * OPUPUT: WARN|ERROR
     * NOTE:max 3 options
     */
        public static String formRegexp(String[] input){
        StringBuilder res = new StringBuilder();
        for(int i = 0;i<input.length;i++){
            if (!input[i].equals("")) {
                res.append(input[i].toUpperCase() + "|");
            }
        }
        res.deleteCharAt(res.length()-1);
        System.out.println(res);
        return res.toString();
    }

}